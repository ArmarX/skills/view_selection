/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::scheduler_example
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/armem/client/plugins.h>
// for writing
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>
#include <RobotAPI/libraries/armem/core/error/mns.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>

#include <armarx/view_selection/gaze_targets.h>

namespace armarx::view_selection::client
{
    class ViewSelection
    {
    public:
        ViewSelection(armarx::armem::client::MemoryNameSystem& mns,
                      std::string providerSegmentName);
        ~ViewSelection();

        void connect();

        void commitGazeTarget(const gaze_targets::GazeTarget& target);
        void commitGazeTargets(const std::vector<gaze_targets::GazeTarget>& targets);

    protected:
    private:
    private:
        // Private member variables go here.
        armarx::armem::client::MemoryNameSystem& memoryNameSystem;
        std::string providerSegmentName;
        const armarx::armem::MemoryID coreSegmentID;
        armarx::armem::client::Writer memoryWriter;
    };

} // namespace armarx::view_selection::client
