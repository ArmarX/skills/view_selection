/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::scheduler_example
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ViewSelection.h"

#include <ArmarXCore/core/time.h>

#include <RobotAPI/libraries/core/FramedPose.h>

#include <armarx/view_selection/memory/constants.h>

namespace armarx::view_selection::client
{

    ViewSelection::ViewSelection(armarx::armem::client::MemoryNameSystem& mns,
                                 std::string providerSegmentName) :
        memoryNameSystem(mns),
        providerSegmentName(providerSegmentName),
        coreSegmentID(memory::constants::ViewMemoryName,
                      memory::constants::GazeTargetCoreSegmentName)
    {
    }

    ViewSelection::~ViewSelection() = default;

    void
    ViewSelection::connect()
    {
        ARMARX_IMPORTANT << "ViewSelection: Waiting for memory '" << coreSegmentID << "' ...";
        try
        {
            memoryWriter = memoryNameSystem.getWriter(coreSegmentID);
            ARMARX_IMPORTANT << "ViewSelection: Connected to memory '" << coreSegmentID << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }
    }

    void
    ViewSelection::commitGazeTarget(const gaze_targets::GazeTarget& target)
    {
        if (not memoryWriter)
        {
            ARMARX_WARNING << "Memory writer is null. Did you forget to call "
                              "ViewSelection::connect() in onConnectComponent()?";
            return;
        }
        if (target.name == "")
        {
            ARMARX_WARNING << "The target name is empty. Did you forget to initialize it?";
            return;
        }

        gaze_targets::arondto::GazeTarget dto;
        toAron(dto, target);

        armem::EntityUpdate update;
        update.entityID =
            coreSegmentID.withProviderSegmentName(providerSegmentName).withEntityName(dto.name);
        update.timeCreated = armarx::Clock::Now();
        update.instancesData = {dto.toAron()};
        memoryWriter.commit(update);
    }

    void
    ViewSelection::commitGazeTargets(const std::vector<gaze_targets::GazeTarget>& targets)
    {
        for (const auto target : targets)
        {
            commitGazeTarget(target);
        }
    }

} // namespace armarx::view_selection::client
