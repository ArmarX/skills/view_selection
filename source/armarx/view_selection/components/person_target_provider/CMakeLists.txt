armarx_add_component(person_target_provider
    ICE_FILES
        ComponentInterface.ice
    ICE_DEPENDENCIES
        ArmarXCoreInterfaces
        # RobotAPIInterfaces
    # ARON_FILES
        # aron/my_type.xml
    SOURCES
        Component.cpp
        Features.cpp
    HEADERS
        Component.h
        Features.h
    DEPENDENCIES
        # ArmarXCore
        ArmarXCore
        ## ArmarXCoreComponentPlugins  # For DebugObserver plugin.
        # ArmarXGui
        ## ArmarXGuiComponentPlugins  # For RemoteGui plugin.
        # RobotAPI
        armem_human
        armem_robot_state
        armarx_view_selection::gaze_targets
        armarx_view_selection::client
        ## RobotAPICore
        ## RobotAPIInterfaces
        ## RobotAPIComponentPlugins  # For ArViz and other plugins.
    # DEPENDENCIES_LEGACY
        ## Add libraries that do not provide any targets but ${FOO_*} variables.
        # FOO
    # If you need a separate shared component library you can enable it with the following flag.
    # SHARED_COMPONENT_LIBRARY
)
