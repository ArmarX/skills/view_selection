/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::person_target_provider
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <vector>

#include <Eigen/Core>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/libraries/armem/client/forward_declarations.h>
#include <RobotAPI/libraries/armem/client/plugins.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>
#include <RobotAPI/libraries/armem/client/plugins/ReaderWriterPlugin.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>

#include <VisionX/libraries/armem_human/client/HumanPoseReader.h>

#include <armarx/view_selection/client/ViewSelection.h>
#include <armarx/view_selection/components/person_target_provider/ComponentInterface.h>
#include <armarx/view_selection/components/person_target_provider/Features.h>

namespace armarx::view_selection::components::person_target_provider
{
    class Component :
        virtual public armarx::Component,
        virtual public ObjectPoseClientPluginUser,
        // virtual public armarx::view_selection::components::person_target_provider::ComponentInterface,
        // virtual public armem::client::PluginUser
        // virtual public armem::client::MemoryListenerInterface
        virtual public armem::ListeningClientPluginUser
    // , virtual public armarx::DebugObserverComponentPluginUser
    // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
    // , virtual public armarx::ArVizComponentPluginUser
    {
    public:
        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();

        Component();

    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        void updateHumanPoses();
        void setTarget(const Eigen::Vector3f& chosenTarget);

        /* (Requires armarx::LightweightRemoteGuiComponentPluginUser.)
        /// This function should be called once in onConnect() or when you
        /// need to re-create the Remote GUI tab.
        void createRemoteGuiTab();

        /// After calling `RemoteGui_startRunningTask`, this function is
        /// called periodically in a separate thread. If you update variables,
        /// make sure to synchronize access to them.
        void RemoteGui_update() override;
        */


    private:
        std::vector<GazeTargetWithFeatures> getHumans();
        std::vector<GazeTargetWithFeatures> getRelevantObjects();

        std::string currentTarget;

        // Private methods go here.

        // Forward declare `Properties` if you used it before its defined.
        // struct Properties;

        /* (Requires the armarx::ArVizComponentPluginUser.)
        /// Draw some boxes in ArViz.
        void drawBoxes(const Properties& p, viz::Client& arviz);
        */


    private:
        armarx::SimplePeriodicTask<>::pointer_type task;
        static const std::string defaultName;


        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            std::string providerName = "OpenNIPointCloudProvider";

            std::string objectProviderDynamic = "sim_track";

            std::string robotName = "Armar6";
            std::string robotBaseFrame = "root";

            float objectDetectionTimeout = 3.f;
            double trackTime = 5.0f;

            std::string targetLayerName = "targets";
        };
        Properties properties;
        /* Use a mutex if you access variables from different threads
         * (e.g. ice functions and RemoteGui_update()).
        std::mutex propertiesMutex;
        */


        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        /// Tab shown in the Remote GUI.
        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            armarx::RemoteGui::Client::LineEdit boxLayerName;
            armarx::RemoteGui::Client::IntSpinBox numBoxes;

            armarx::RemoteGui::Client::Button drawBoxes;
        };
        RemoteGuiTab tab;
        */


        /* (Requires the armarx::ArVizComponentPluginUser.)
         * When used from different threads, an ArViz client needs to be synchronized.
        /// Protects the arviz client inherited from the ArViz plugin.
        std::mutex arvizMutex;
        */
        VirtualRobot::RobotPtr robot;

        armem::client::plugins::ReaderWriterPlugin<armem::robot_state::VirtualRobotReader>*
            virtualRobotReaderPlugin = nullptr;
        armem::client::plugins::ReaderWriterPlugin<armem::human::client::Reader>*
            humanPoseReaderPlugin = nullptr;

        client::ViewSelection viewSelection;
    };

} // namespace armarx::view_selection::components::person_target_provider
