/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::person_target_provider
 * @author     armar-user ( armar-user at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <cstddef>

#include <Eigen/Core>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/Clock.h>
#include <ArmarXCore/core/time/StopWatch.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include "RobotAPI/libraries/core/FramedPose.h"


namespace armarx::view_selection::components::person_target_provider
{

    const std::string Component::defaultName = "person_target_provider";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)


        // Add a required property. (The component won't start without a value being set.)
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optionalproperty.
        // def->optional(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");
        // def->optional(properties.numBoxes, "p.box.Number", "Number of boxes to draw in ArViz.");
        def->optional(properties.providerName, "p.box.ProviderName", "OpenNIPointCloudProvider");
        def->optional(properties.objectProviderDynamic, "p.objectProviderDynamic", "");
        def->optional(properties.trackTime,
                      "p.trackTime",
                      "Roughly the expected average tracking time of an object in seconds.");
        def->optional(properties.robotName, "p.robotName", "");
        return def;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);
    }


    void
    Component::onConnectComponent()
    {
        // Do things after connecting to topics and components.

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
        */
        robot = virtualRobotReaderPlugin->get().getRobot(properties.robotName);

        // Control head
        viewSelection.connect();

        // Setup task
        task = new armarx::SimplePeriodicTask<>([&]() { updateHumanPoses(); }, 250);
        task->start();
    }


    void
    Component::onDisconnectComponent()
    {
        task->stop();
        task = nullptr;
    }


    void
    Component::onExitComponent()
    {
    }


    std::optional<objpose::ObjectPose>
    findObject(const objpose::ObjectPoseSeq& objectPoses, const armarx::ObjectID& objectID)
    {
        const auto matchesId = [&objectID](const objpose::ObjectPose& objectPose) -> bool
        { return objectPose.objectID == objectID; };

        const auto it = std::find_if(objectPoses.begin(), objectPoses.end(), matchesId);
        if (it != objectPoses.end())
        {
            return *it;
        }

        return std::nullopt;
    }

    std::vector<GazeTargetWithFeatures>
    Component::getHumans()
    {
        armarx::armem::human::client::Reader::Query query{
            .providerName = "OpenNIPointCloudProvider",
            .timestamp = armarx::Clock::Now(),
            .maxAge = armarx::Duration::Seconds(3),
        };
        armarx::core::time::StopWatch sw;
        auto result = humanPoseReaderPlugin->get().query(query);
        ARMARX_INFO << "SP3 took " << sw.stopAndReset();
        ARMARX_VERBOSE << result.humanPoses.size();

        std::vector<GazeTargetWithFeatures> gazeTargets(result.humanPoses.size());
        for (size_t i = 0; i < result.humanPoses.size(); i++)
        {
            auto human = result.humanPoses[i];

            // TODO check whether "Head" keypoint is available
            gazeTargets[i].position = human.keypoints["Head"].positionGlobal.value().toEigen();
            gazeTargets[i].trackingName = human.humanTrackingId.value_or("default_human");
            gazeTargets[i].ishuman = true;
        }

        return gazeTargets;
    }

    std::vector<GazeTargetWithFeatures>
    Component::getRelevantObjects()
    {

        const armarx::objpose::ObjectPoseSeq objectPosesDynamic =
            armarx::ObjectPoseClientPluginUser::getObjectPosesByProvider(
                properties.objectProviderDynamic);

        // TODO this should be variable. Likely, all objects from provider are interesting
        const std::set<armarx::ObjectID> relevantObjects = {
            armarx::ObjectID("Kitchen", "vanilla-cookies"),
            armarx::ObjectID("YCB", "003_cracker_box"),
            armarx::ObjectID("Kitchen", "nesquik"),
            armarx::ObjectID("Maintenance", "spraybottle"),
        };

        std::vector<GazeTargetWithFeatures> gazeTargets;
        for (size_t i = 0; i < objectPosesDynamic.size(); i++)
        {
            auto object = objectPosesDynamic[i];
            if (relevantObjects.find(object.objectID) != relevantObjects.end())
            {
                GazeTargetWithFeatures newTarget;
                newTarget.position = Eigen::Isometry3f(object.objectPoseGlobal).translation();
                newTarget.trackingName = object.objectID.str();
                newTarget.ishuman = false;
                gazeTargets.push_back(newTarget);
            }
        }

        return gazeTargets;
    }

    void
    Component::setTarget(const Eigen::Vector3f& chosenTarget)
    {
        if (chosenTarget.norm() < 1e-5)
        {
            return;
        }

        gaze_targets::GazeTarget target;
        target.name = "tracking_target";
        target.position = armarx::FramedPosition(chosenTarget, GlobalFrame, robot->getName());

        target.priority =
            gaze_targets::TargetPriority(gaze_targets::AttentionType::StimulusDriven, 10'000);
        target.duration = armarx::Duration::Seconds(30);
        target.creationTimestamp = armarx::Clock::Now();

        viewSelection.commitGazeTarget(target);
    }

    void
    Component::updateHumanPoses()
    {
        // Get robot position
        ARMARX_CHECK(
            virtualRobotReaderPlugin->get().synchronizeRobot(*robot, armarx::Clock::Now()));
        Eigen::Vector3f globalPose = robot->getRootNode()->getGlobalPosition();
        ARMARX_VERBOSE << globalPose;


        // Find relevant targets
        std::vector<GazeTargetWithFeatures> targets = getHumans();
        {
            std::vector<GazeTargetWithFeatures> additional_targets = getRelevantObjects();
            targets.insert(targets.end(),
                           std::make_move_iterator(additional_targets.begin()),
                           std::make_move_iterator(additional_targets.end()));
        }

        // Calculate hotness values, hotter means more likely to be tracked
        double totalHotness = 1e-5;
        double currentTargetHotness = 0;
        Eigen::Vector3f currentTargetPosition;
        for (auto target : targets)
        {
            ARMARX_VERBOSE << "Object ID '" << target.trackingName << "' with hotness "
                           << target.calculateHotness(globalPose);
            totalHotness += target.calculateHotness(globalPose);
            if (currentTarget == target.trackingName)
            {
                currentTargetHotness = target.calculateHotness(globalPose);
                currentTargetPosition = target.position;
            }
        }

        // Decide if a new object is going to be tracked
        double lambda = properties.trackTime * 4 - 1;
        if (lambda < 0)
        {
            ARMARX_WARNING << "Value for trackTime is too small, choose at least 0.25!";
            setTarget(currentTargetPosition);
            return;
        }
        double newObjectProbability = 1.0 / (1.0 + (currentTargetHotness / totalHotness * lambda));
        ARMARX_VERBOSE << "New target lambda " << lambda << " prob " << newObjectProbability;
        if (((rand() % 10'000'000) / 10'000'000.0) > newObjectProbability)
        {
            setTarget(currentTargetPosition);
            return; // No new object is going to be tracked
        }
        ARMARX_VERBOSE << "New target is going to be chosen";

        // Decide which Object should be tracked
        double randomValue = (rand() % 10'000'000) / 10'000'000.0;
        double cumulatedHotness = 1e-5; // Add a small delta for floating point issues
        for (auto chosenTarget : targets)
        {
            cumulatedHotness += chosenTarget.calculateHotness(globalPose) / totalHotness;
            if (cumulatedHotness >= randomValue)
            {
                ARMARX_INFO << "Chose " << chosenTarget.trackingName << " at "
                            << chosenTarget.position;
                // Set new Target
                setTarget(chosenTarget.position);
                currentTarget = chosenTarget.trackingName;
                return;
            }
        }
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }

    Component::Component() : viewSelection(memoryNameSystem(), getName())
    {
        addPlugin(virtualRobotReaderPlugin);
        addPlugin(humanPoseReaderPlugin);
    }

    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    
    */


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::view_selection::components::person_target_provider
