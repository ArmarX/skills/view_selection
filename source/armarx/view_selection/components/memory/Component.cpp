/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::memory
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Component.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <armarx/view_selection/gaze_targets/aron/GazeTarget.aron.generated.h>
#include <armarx/view_selection/memory/constants.h>

// Include headers you only need in function definitions in the .cpp.

namespace armarx::view_selection::components::memory
{

    const std::string Component::defaultName = "ViewMemory";

    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());
        workingMemory().name() = ::armarx::view_selection::memory::constants::ViewMemoryName;
        return def;
    }

    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.
        workingMemory().addCoreSegment(
            ::armarx::view_selection::memory::constants::GazeTargetCoreSegmentName,
            armarx::view_selection::gaze_targets::arondto::GazeTarget::ToAronType());
    }

    void
    Component::onConnectComponent()
    {
    }

    void
    Component::onDisconnectComponent()
    {
    }

    void
    Component::onExitComponent()
    {
    }

    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }

    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }

    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::view_selection::components::memory
