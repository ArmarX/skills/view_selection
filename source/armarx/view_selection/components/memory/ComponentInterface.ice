/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    view_selection::view_memory
 * author     Johann Mantel ( j-mantel at gmx dot net )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
 */

#pragma once
#include <RobotAPI/interface/armem/server/MemoryInterface.ice>

module armarx {
  module view_selection {
    module components {
      module memory {

        interface ComponentInterface extends
            armarx::armem::server::MemoryInterface{
                // Define your interface here.
            };
      };
    };
  };
};
