
#pragma once

#include <ArmarXCore/interface/core/time.ice>
#include <RobotAPI/interface/armem/client/MemoryListenerInterface.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>

#include <armarx/view_selection/gaze_targets/ice/GazeTargets.ice>

module armarx {
  module view_selection {
    module gaze_scheduler {
      interface GazeSchedulerInterface extends
          armarx::armem::client::MemoryListenerInterface
      {
        //void fixateFor(string targetName, FramedPositionBase target,
        //               armarx::view_selection::gaze_targets::dto::TargetPriority priority,
        //               armarx::core::time::dto::Duration duration,
        //               bool keepInQueue);

        void resetPriorityQueue();
      };
    };
  };
};
