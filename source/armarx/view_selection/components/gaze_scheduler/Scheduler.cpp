/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::gaze_control
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Scheduler.h"

#include <chrono>
#include <thread>

#include <SimoxUtility/color/Color.h>

#include "ArmarXCore/core/logging/Logging.h"
#include <ArmarXCore/core/time/ice_conversions.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client/query.h>
#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/core.h>

#include <armarx/view_selection/gaze_controller/aron/GazeControllerConfig.aron.generated.h>
#include <armarx/view_selection/gaze_controller/aron_conversions.h>
#include <armarx/view_selection/gaze_targets.h>
#include <armarx/view_selection/gaze_targets/AttentionType.h>
#include <armarx/view_selection/gaze_targets/TargetPriority.h>
#include <armarx/view_selection/memory/constants.h>

namespace armarx::view_selection::gaze_scheduler
{

    const std::string Scheduler::defaultName = memory::constants::GazeSchedulerProviderName;

    Scheduler::Scheduler()
    {
        addPlugin(virtualRobotReaderPlugin);
    }

    armarx::PropertyDefinitionsPtr
    Scheduler::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());
        def->optional(properties.controllerType,
                      "controllerType",
                      "The type of NJointController to use, e.g. GazeController");
        def->optional(properties.controllerName,
                      "controllerName",
                      "The name under which the controller is registered");
        def->optional(
            properties.controllerConfig.yawAngleTolerance,
            "properties.controllerConfig.yawAngleTolerance",
            "The yaw angle distance at which the controller considers the target as reached");
        def->optional(
            properties.controllerConfig.pitchAngleTolerance,
            "properties.controllerConfig.pitchAngleTolerance",
            "The pitch angle distance at which the controller considers the target as reached");
        def->optional(properties.controllerConfig.abortIfUnreachable,
                      "properties.controllerConfig.abortIfUnreachable",
                      "If true, the controller drops a target if it gets out of reach."
                      "If false, the controller stays at the jointlimits and keeps the target");
        def->optional(properties.defaultTarget,
                      "properties.defaultTarget",
                      "Local coordinates to Position where the robot looks in idle phases.");
        return def;
    }

    void
    Scheduler::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.
    }

    void
    Scheduler::onConnectComponent()
    {
        // Do things after connecting to topics and components.
        ARMARX_INFO << "Connecting controller.";
        // Load the controllers that we are using here.

        getControlComponentPlugin().getRobotUnitPlugin().getRobotUnit()->loadLibFromPackage(
            "armarx_view_selection", "libarmarx_view_selection_gaze_controller.so");

        // obtain info about robot
        const std::string robotName = getControlComponentPlugin()
                                          .getRobotUnitPlugin()
                                          .getRobotUnit()
                                          ->getKinematicUnit()
                                          ->getRobotName();

        robot = virtualRobotReaderPlugin->get().getRobot(
            robotName,
            armarx::DateTime::Invalid(),
            VirtualRobot::RobotIO::RobotDescription::eStructure);
        ARMARX_CHECK_NOT_NULL(robot);

        //populate the robot
        RobotNodeNames robotNodeNames = getRobotNodeNames(robotName);

        gaze_controller::Config config;
        config.params.yawNodeName = robotNodeNames.yawNodeName;
        config.params.pitchNodeName = robotNodeNames.pitchNodeName;
        config.params.cameraNodeName = robotNodeNames.cameraNodeName;

        // initialize controller
        ARMARX_INFO << "Initializing controller";
        {
            ARMARX_TRACE;
            auto builder =
                getControlComponentPlugin()
                    .createControllerBuilder<
                        armarx::view_selection::common::ControllerType::GazeController>();

            //const armarx::PackagePath configPath(
            //    "armarx_view_selection", "controller_config/GazeController/default.json");

            ARMARX_INFO << "Initializing controller now";
            // auto ctrlWrapper = builder.withNodeSet("Head").withConfig(configPath.toSystemPath()).create();

            gaze_controller::arondto::Config dtoConfig;
            toAron(dtoConfig, config);

            ARMARX_IMPORTANT << "builder.withNodeSet().withConfig";
            builder.withNodeSet("Head").withConfig(dtoConfig);

            ARMARX_IMPORTANT << "builder.create()";
            auto ctrlWrapper = builder.create();
            ARMARX_TRACE;
            ARMARX_CHECK(ctrlWrapper.has_value());

            ARMARX_INFO << "Successfully created controller";
            ctrl.emplace(std::move(ctrlWrapper.value()));
        }

        ARMARX_TRACE;
        ARMARX_CHECK(ctrl.has_value());

        //Properties are loaded OK
        ARMARX_INFO << "Activating controller";
        ctrl->activate();

        // subscribe to memory updates
        {
            namespace constants = ::armarx::view_selection::memory::constants;
            const armem::MemoryID coreSegmentID(constants::ViewMemoryName,
                                                constants::GazeTargetCoreSegmentName);
            memoryNameSystem().subscribe(coreSegmentID,
                                         [&](const std::vector<armem::MemoryID>& updatedSnapshotIDs)
                                         { onMemoryUpdated(updatedSnapshotIDs); });
        }
        resetPriorityQueue();
        submitIdleTarget();

        task = new armarx::SimplePeriodicTask<>([&]() { checkTargetDeadlinePeriodically(); }, 100);
        task->start();
    }

    void
    Scheduler::onDisconnectComponent()
    {
        ARMARX_INFO << "Disconnecting controller.";
    }

    void
    Scheduler::onExitComponent()
    {
        // delete the controller wrapper => controller will be deactivated and deleted
        ctrl.reset();
    }

    void
    Scheduler::resetPriorityQueue(const Ice::Current&)
    {
        ARMARX_INFO << "Resetting PriorityQueue, old requests are discarded";
        std::scoped_lock<std::mutex> targetLock(targetMutex);
        requestedTargets.clear();
    }

    void
    Scheduler::checkTargetDeadlinePeriodically()
    {
        bool deadlineViolation = false;
        {
            std::scoped_lock<std::mutex> targetLock(targetMutex);
            if (currentDeadline.isValid() && currentDeadline <= armarx::Clock::Now())
            {
                deadlineViolation = true;
                ARMARX_VERBOSE << "Deadline reached, deactivating target.";
                currentDeadline = DateTime::Invalid();
                activeTarget = false;
            }
        }
        if (deadlineViolation)
        {
            scheduleNextTarget();
        }
    }

    void
    Scheduler::submitIdleTarget()
    {
        FramedPosition viewCenter(
            properties.defaultTarget, robot->getRootNode()->getName(), robot->getName());
        auto infinite = armarx::Duration::MilliSeconds(-1);
        gaze_targets::TargetPriority priority(gaze_targets::AttentionType::RandomEvent, 0.0);
        // this target has lowest priority and will never be removed from the queue
        {
            std::scoped_lock<std::mutex> targetLock(targetMutex);
            requestedTargets.emplace(
                gaze_targets::GazeTarget("idle", viewCenter, priority, infinite, true));
        }
        scheduleNextTarget();
    }

    void
    Scheduler::visualizeActiveTarget(const gaze_targets::GazeTarget& target)
    {
        virtualRobotReaderPlugin->get().synchronizeRobot(*robot, armarx::Clock::Now());

        const auto globalPosition = target.position.toGlobalEigen(robot);

        std::map<gaze_targets::AttentionType, simox::Color> attentionTypeColor{
            {gaze_targets::AttentionType::RandomEvent, simox::Color::gray()},
            {gaze_targets::AttentionType::StimulusDriven, simox::Color::blue()},
            {gaze_targets::AttentionType::TaskDriven, simox::Color::red()}};

        // TODO include priority into coloring

        auto l = arviz.layer("active_target");
        l.add(armarx::viz::Sphere("target")
                  .position(globalPosition)
                  .radius(100)
                  .color(attentionTypeColor.at(target.priority.attentionType)));
        arviz.commit(l);
    }

    void
    Scheduler::updateControllerTarget(const gaze_targets::GazeTarget& gazeTarget)
    {
        ARMARX_INFO << "Sending new target to the controller " << gazeTarget;
        armarx::view_selection::gaze_targets::arondto::GazeTarget dto;
        toAron(dto, gazeTarget);

        ctrl->config.targets.gazeTarget = dto;
        ctrl->updateConfig();
    }

    void
    Scheduler::submitToQueue(const gaze_targets::GazeTarget& target)
    {
        {
            std::scoped_lock<std::mutex> targetLock(targetMutex);
            // replace the target if it is currently active
            if (activeTarget)
            {
                if (currentTarget.name == target.name)
                {
                    ARMARX_VERBOSE << "Updating active target: " << target.name;
                    submitControlTarget(target);

                    return;
                }
            }
            // remove target if found in queue
            for (auto it = requestedTargets.cbegin(); it != requestedTargets.cend(); it++)
            {
                if (it->name == target.name)
                {
                    ARMARX_VERBOSE << "Updating targetRequest: " << target.name;
                    requestedTargets.erase(it);
                    break;
                }
            }
            // multiset automatically ensures order
            requestedTargets.emplace(target);
        }
        scheduleNextTarget();
    }

    void
    Scheduler::scheduleNextTarget()
    {
        std::scoped_lock<std::mutex> targetLock(targetMutex);
        if (requestedTargets.empty())
        {
            ARMARX_INFO << "No requests, nothing to do.";
            return;
        }
        else
        {
            // reference to targetRequest with highest priority
            // this will be only used in one of the if-else cases so that they do not
            // influence each other
            auto nextTarget = requestedTargets.cbegin();

            if (!activeTarget)
            {
                ARMARX_INFO << "No currently active Targets, submitting new target: "
                            << *nextTarget;

                submitControlTarget(*nextTarget);

                // keep or erase target after submission based on keepInQueue
                if (nextTarget->keepInQueue)
                {
                    ARMARX_VERBOSE << "The target is kept in the queue.";
                }
                else
                {
                    requestedTargets.erase(nextTarget);
                    ARMARX_VERBOSE << "The target has been removed from the queue.";
                }
            }
            else
            {
                if (nextTarget->priority > currentTarget.priority)
                {
                    ARMARX_INFO << "requested Target priority > current Target priority ("
                                << nextTarget->priority << " > " << currentTarget.priority << ")";
                    // replaced target needs to be stopped properly, then next target will be
                    // scheduled automatically
                    submitControlTarget(*nextTarget);
                }
                else
                {
                    ARMARX_INFO << "current Target has higher priority (" << nextTarget->priority
                                << " <= " << currentTarget.priority
                                << ") waiting until it is reached";
                }
            }
        }
    }

    void
    Scheduler::submitControlTarget(const gaze_targets::GazeTarget& target)
    {
        if (target.duration.toMicroSeconds() < 0)
        {
            // infinite duration
            currentDeadline = armarx::DateTime::Invalid();
        }
        else
        {
            currentDeadline = armarx::Clock::Now() + target.duration;
        }
        currentTarget = target;
        activeTarget = true;

        updateControllerTarget(target);

        visualizeActiveTarget(target);
    }


    void
    Scheduler::onMemoryUpdated(const std::vector<armarx::armem::MemoryID>& updatedSnapshotIDs)
    {
        namespace armem = armarx::armem;
        namespace constants = ::armarx::view_selection::memory::constants;
        const armem::MemoryID coreSegmentID(constants::ViewMemoryName,
                                            constants::GazeTargetCoreSegmentName);
        const armem::MemoryID schedulerProviderID(constants::ViewMemoryName,
                                                  constants::GazeTargetCoreSegmentName,
                                                  constants::GazeSchedulerProviderName);
        armem::client::Reader reader = memoryNameSystem().getReader(coreSegmentID);

        ARMARX_INFO << "Received memory update: " << updatedSnapshotIDs; 

        const auto result = reader.queryMemoryIDs(updatedSnapshotIDs);
        if (result.success)
        {
            const armem::wm::Memory& memory = result.memory;
            for (const auto& memId : updatedSnapshotIDs)
            {
                if (not armem::contains(schedulerProviderID, memId))
                {
                    // foreign memory updates are targetRequests
                    ARMARX_VERBOSE << "new Request received: " << memId.str();
                    const auto* snapshot = memory.findLatestInstance(memId);

                    if (snapshot != nullptr)
                    {
                        gaze_targets::GazeTarget gazeTarget =
                            armarx::aron::fromAron<gaze_targets::GazeTarget>(
                                gaze_targets::arondto::GazeTarget::FromAron(snapshot->data()));

                        submitToQueue(gazeTarget);
                    }
                    else
                    {
                        ARMARX_INFO << "No memory found with id:" << memId;
                    }
                }
            }
        }
        else
        {
            ARMARX_INFO << "Query failed: " << result.errorMessage;
        }
    }

    Scheduler::RobotNodeNames
    Scheduler::getRobotNodeNames(std::string robotName)
    {
        RobotNodeNames nodeNames;
        if (robotName == "Armar6")
        {
            nodeNames.cameraFrameName = "DepthCamera";
            nodeNames.yawNodeName = "Neck_1_Yaw";
            nodeNames.pitchNodeName = "Neck_2_Pitch";
            nodeNames.cameraNodeName = "DepthCamera";
            nodeNames.torsoNodeName = "TorsoJoint";
        }
        else
        {
            ARMARX_WARNING << "robotName not known, using fallback values";
        }
        return nodeNames;
    }

    std::string
    Scheduler::getDefaultName() const
    {
        return Scheduler::defaultName;
    }

    std::string
    Scheduler::GetDefaultName()
    {
        return Scheduler::defaultName;
    }

    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Scheduler, Scheduler::GetDefaultName());

} // namespace armarx::view_selection::gaze_scheduler
