/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::gaze_scheduler
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <queue>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/time.h>

#include "RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h"
#include "RobotAPI/libraries/armem/client/plugins/ReaderWriterPlugin.h"
#include "RobotAPI/libraries/armem_robot/types.h"
#include "RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h"
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client/plugins/ListeningPluginUser.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <armarx/control/client/ComponentPlugin.h>
#include <armarx/control/client/ControllerWrapper.h>

#include "armarx/view_selection/gaze_targets/GazeTarget.h"
#include <armarx/view_selection/common/controller_types.h>
#include <armarx/view_selection/components/gaze_scheduler/GazeSchedulerInterface.h>
#include <armarx/view_selection/gaze_controller/GazeController.h>
#include <armarx/view_selection/gaze_controller/controller_descriptions.h>
#include <armarx/view_selection/gaze_targets.h>

namespace armarx::view_selection::gaze_scheduler
{

    class Scheduler :
        virtual public armarx::Component,
        virtual public armarx::view_selection::gaze_scheduler::GazeSchedulerInterface,
        virtual public armarx::RobotUnitComponentPluginUser,
        virtual public armarx::armem::ListeningClientPluginUser,
        virtual public armarx::control::client::ComponentPluginUser,
        virtual public armarx::ArVizComponentPluginUser
    {
    public:
        Scheduler();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
        /// Get the component's default name.
        static std::string GetDefaultName();


        // from GazeSchedulerInterface
        void resetPriorityQueue(const Ice::Current& c = ::Ice::Current()) override;

        // GazeControllerListener
        // void reportGazeTarget(const ::armarx::aron::data::dto::DictPtr& dto,
        //                       const Ice::Current&) override;

        // MemoryListeningClient
        void onMemoryUpdated(const std::vector<armarx::armem::MemoryID>& updatedSnapshotIDs);

    protected:
        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        void handleTargetUpdate(const gaze_targets::GazeTarget& memoryTarget);

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        struct RobotNodeNames
        {
            std::string cameraFrameName = "DepthCamera";
            std::string yawNodeName = "Neck_1_Yaw";
            std::string pitchNodeName = "Neck_2_Pitch";
            std::string cameraNodeName = "DepthCamera";
            std::string torsoNodeName = "TorsoJoint";
        };
        // return node names for given robot
        // TODO: implement this function for other robots than Armar6
        RobotNodeNames getRobotNodeNames(std::string robotName);

        //check whether the current target is out of time and reschedule
        void checkTargetDeadlinePeriodically();

        void submitControlTarget(const gaze_targets::GazeTarget& target);

        // submit next target from queue based on priority and preemption attributes
        // WARNING: can only be safely called after acquiring the targetLock
        void scheduleNextTarget();

        // an irreplaceable default target with low priority used if no other
        // targets are submitted
        void submitIdleTarget();

        void submitToQueue(const gaze_targets::GazeTarget& target);

        void visualizeActiveTarget(const gaze_targets::GazeTarget& target);

    private:
        void updateControllerTarget(const gaze_targets::GazeTarget& gazeTarget);

        static const std::string defaultName;

        // Private member variables go here.

        std::optional<armarx::control::client::ControllerWrapper<
            armarx::view_selection::common::ControllerType::GazeController>>
            ctrl;

        // target managemant needs to be synchronized
        std::mutex targetMutex;
        std::multiset<gaze_targets::GazeTarget, std::greater<gaze_targets::GazeTarget>>
            requestedTargets;

        std::atomic<bool> activeTarget = false;
        gaze_targets::GazeTarget currentTarget;
        armarx::DateTime currentDeadline = DateTime::Invalid();
        armarx::SimplePeriodicTask<>::pointer_type task;

        struct ControllerConfigParams
        {
            float yawAngleTolerance = 0.005;
            float pitchAngleTolerance = 0.005;
            bool abortIfUnreachable = false;
        };
        struct Properties
        {
            std::string controllerType =
                common::ControllerTypeNames.to_name(common::ControllerType::GazeController);
            std::string controllerName =
                GetDefaultName() + "_" +
                common::ControllerTypeNames.to_name(common::ControllerType::GazeController);
            ControllerConfigParams controllerConfig;

            Eigen::Vector3f defaultTarget = Eigen::Vector3f(0, 1500, 1500);
        };
        Properties properties;

        armem::client::plugins::ReaderWriterPlugin<armem::robot_state::VirtualRobotReader>*
            virtualRobotReaderPlugin = nullptr;

        VirtualRobot::RobotPtr robot = nullptr;
    };

} // namespace armarx::view_selection::gaze_scheduler
