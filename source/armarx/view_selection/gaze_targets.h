/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::gaze_targets
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once
#include "gaze_targets/AttentionType.h"
#include "gaze_targets/GazeTarget.h"
#include "gaze_targets/TargetPriority.h"
#include "gaze_targets/TargetStatus.h"
#include "gaze_targets/aron_conversions.h"
#include "gaze_targets/ice_conversions.h"

namespace armarx
{
    /**
 * @defgroup Library-gaze_targets gaze_targets
 * @ingroup view_selection
 * A description of the library gaze_targets.
 *
 * @class gaze_targets
 * @ingroup Library-gaze_targets
 * @brief Brief description of class gaze_targets.
 *
 * Detailed description of class gaze_targets.
 */
    class gaze_targets
    {
    public:
    };

} // namespace armarx
