#pragma once

#include <SimoxUtility/meta/enum/EnumNames.hpp>

namespace armarx::view_selection::common
{

    enum class ControllerType
    {
        GazeController
    };

    constexpr const char* GazeControllerName = "GazeController";

    inline const simox::meta::EnumNames<ControllerType> ControllerTypeNames{
        {ControllerType::GazeController, GazeControllerName}};


} // namespace armarx::view_selection::common
