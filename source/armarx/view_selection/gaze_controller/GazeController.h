
/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection
 * @author     Johann Mantel
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/PIDController.h>

#include <armarx/control/interface/ConfigurableNJointControllerInterface.h>

#include <armarx/view_selection/gaze_controller/aron/GazeControllerConfig.aron.generated.h>
#include <armarx/view_selection/gaze_targets.h>
#include <armarx/view_selection/gaze_targets/GazeTarget.h>

namespace armarx::view_selection::gaze_controller
{
    TYPEDEF_PTRS_HANDLE(GazeController);

    struct Params
    {
        std::string yawNodeName;
        std::string pitchNodeName;
        std::string cameraNodeName;
    };

    struct Targets
    {
        gaze_targets::GazeTarget gazeTarget;
    };

    struct Config
    {
        Params params;
        Targets targets;
    };


    /**
    * @defgroup Component-GazeControl GazeControl
    * @ingroup ActiveVision-Components
    * A description of the component GazeControl.
    *
    * @class GazeControl
    * @ingroup Component-GazeControl
    * @brief Brief description of class GazeControl.
    *
    * Detailed description of class GazeControl.
    */
    class GazeController :
        virtual public NJointControllerWithTripleBuffer<Config>,
        virtual public armarx::control::ConfigurableNJointControllerInterface
    {
    public:
        using ConfigPtrT = control::ConfigurableNJointControllerConfigPtr;

        GazeController(RobotUnitPtr robotUnit,
                       const NJointControllerConfigPtr& config,
                       const VirtualRobot::RobotPtr& robot);

        ~GazeController() override;

        void updateConfig(const ::armarx::aron::data::dto::DictPtr& dto,
                          const Ice::Current& iceCurrent = Ice::emptyCurrent) override;


    protected:
        // NJointControllerInterface interface
        void onInitNJointController() override;

        // NJointControllerInterface interface
        void onConnectNJointController() override;

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;

        // NJointController interface
        void onPublish(const SensorAndControl&,
                       const DebugDrawerInterfacePrx&,
                       const DebugObserverInterfacePrx&) override;

        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    private:
        // void publishTarget(const armarx::view_selection::gaze_targets::GazeTarget& target);
        void removeTarget();

        // rt variables
        VirtualRobot::RobotPtr _rtRobot;
        VirtualRobot::RobotNodePtr _rtYawNode;
        VirtualRobot::RobotNodePtr _rtPitchNode;
        VirtualRobot::RobotNodePtr _rtCameraNode;
        // VirtualRobot::RobotNodePtr _rtTorsoNode;

        armarx::ControlTarget1DoFActuatorPosition* _rtPitchCtrlTarget;
        armarx::ControlTarget1DoFActuatorPosition* _rtYawCtrlTarget;
        float* _rtPitchCtrlPos;
        float* _rtYawCtrlPos;

        // publishing
        mutable std::recursive_mutex _tripRt2NonRtMutex;
        TripleBuffer<armarx::view_selection::gaze_targets::GazeTarget> _tripRt2NonRt;

        std::atomic<float> _publishCurrentPitchAngle = 0.0;
        std::atomic<float> _publishCurrentYawAngle = 0.0;
        std::atomic<float> _publishTargetPitchAngle = 0.0;
        std::atomic<float> _publishTargetYawAngle = 0.0;
    };
} // namespace armarx::view_selection::gaze_controller
