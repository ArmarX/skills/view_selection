/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "aron_conversions.h"

#include <armarx/view_selection/gaze_controller/GazeController.h>
#include <armarx/view_selection/gaze_controller/aron/GazeControllerConfig.aron.generated.h>
#include <armarx/view_selection/gaze_targets/aron_conversions.h>

namespace armarx::view_selection::gaze_controller
{

    // Config

    void
    fromAron(const arondto::Config& dto, Config& bo)
    {
        fromAron(dto.params, bo.params);
        fromAron(dto.targets, bo.targets);
    }

    void
    toAron(arondto::Config& dto, const Config& bo)
    {
        toAron(dto.params, bo.params);
        toAron(dto.targets, bo.targets);
    }

    // Params

    void
    fromAron(const arondto::Params& dto, Params& bo)
    {
        bo = Params{.yawNodeName = dto.yawNodeName,
                    .pitchNodeName = dto.pitchNodeName,
                    .cameraNodeName = dto.cameraNodeName};
    }

    void
    toAron(arondto::Params& dto, const Params& bo)
    {
        dto.yawNodeName = bo.yawNodeName;
        dto.pitchNodeName = bo.pitchNodeName;
        dto.cameraNodeName = bo.cameraNodeName;
    }

    // Targets

    void
    fromAron(const arondto::Targets& dto, Targets& bo)
    {
        fromAron(dto.gazeTarget, bo.gazeTarget);
    }

    void
    toAron(arondto::Targets& dto, const Targets& bo)
    {
        toAron(dto.gazeTarget, bo.gazeTarget);
    }

} // namespace armarx::view_selection::gaze_controller
