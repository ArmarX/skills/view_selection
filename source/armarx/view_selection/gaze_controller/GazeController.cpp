
#include "GazeController.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/interface/observers/VariantBase.h>

#include <RobotAPI/components/units/RobotUnit/ControlModes.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/util/ControlThreadOutputBuffer.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <armarx/view_selection/common/controller_types.h>
#include <armarx/view_selection/gaze_controller/aron/GazeControllerConfig.aron.generated.h>
#include <armarx/view_selection/gaze_controller/aron_conversions.h>

namespace armarx::view_selection::gaze_controller
{

    const armarx::NJointControllerRegistration<GazeController> RegistrationControllerGazeController(
        common::ControllerTypeNames.to_name(common::ControllerType::GazeController));

    GazeController::GazeController(armarx::RobotUnitPtr robotUnit,
                                   const NJointControllerConfigPtr& config,
                                   const VirtualRobot::RobotPtr&)
    {
        ARMARX_RT_LOGF("Creating gaze controller");
        // config
        ConfigPtrT cfg = ConfigPtrT::dynamicCast(config);
        ARMARX_CHECK_EXPRESSION(cfg);
        ARMARX_CHECK_EXPRESSION(!cfg->nodeSetName.empty());

        ARMARX_CHECK_EXPRESSION(robotUnit);

        const auto configData = arondto::Config::FromAron(cfg->config);

        // robot
        ARMARX_RT_LOGF("Setting up nodes");
        {
            _rtRobot = useSynchronizedRtRobot();
            ARMARX_CHECK_NOT_NULL(_rtRobot);
            _rtCameraNode = _rtRobot->getRobotNode(configData.params.cameraNodeName);
            _rtPitchNode = _rtRobot->getRobotNode(configData.params.pitchNodeName);
            _rtYawNode = _rtRobot->getRobotNode(configData.params.yawNodeName);
            // _rtTorsoNode = _rtRobot->getRobotNode(_config->torsoNodeName);
            ARMARX_CHECK_NOT_NULL(_rtCameraNode);
            ARMARX_CHECK_NOT_NULL(_rtPitchNode);
            ARMARX_CHECK_NOT_NULL(_rtYawNode);
            // ARMARX_CHECK_NOT_NULL(_rtTorsoNode);
            // joint positions to be controlled
            _rtPitchCtrlTarget = useControlTarget<armarx::ControlTarget1DoFActuatorPosition>(
                configData.params.pitchNodeName, ControlModes::Position1DoF);
            ARMARX_CHECK_NOT_NULL(_rtPitchCtrlTarget);
            //_rtPitchCtrlPos = &(_pitchCtrlTarget->position);
            _rtYawCtrlTarget = useControlTarget<armarx::ControlTarget1DoFActuatorPosition>(
                configData.params.yawNodeName, ControlModes::Position1DoF);
            ARMARX_CHECK_NOT_NULL(_rtYawCtrlTarget);
            //_rtYawCtrlPos = &(yawCtrlTarget->position);
        }

        ARMARX_RT_LOGF("Nodes set up successfully");
    }

    GazeController::~GazeController() = default;

    void
    GazeController::updateConfig(const ::armarx::aron::data::dto::DictPtr& dto,
                                 const Ice::Current&  /*iceCurrent*/)
    {
        // ARMARX_VERBOSE << "Controller::updateConfig";

        auto updateConfigDto = arondto::Config::FromAron(dto);

        Config config;
        fromAron(updateConfigDto, config);

        setControlStruct(config);
    }

    void
    GazeController::onInitNJointController()
    {
    }

    void
    GazeController::onConnectNJointController()
    {
    }

    std::string
    GazeController::getClassName(const Ice::Current&) const
    {
        return common::ControllerTypeNames.to_name(common::ControllerType::GazeController);
    }

    void
    GazeController::rtRun(const IceUtil::Time&  /*sensorValuesTimestamp*/,
                          const IceUtil::Time&  /*timeSinceLastIteration*/)
    {
        auto target = rtGetControlStruct().targets.gazeTarget.position;

        if(target.frame.empty())
        {
            target.frame = GlobalFrame;
        }

        // calculate inverse kinematic
        const float currentYawAngle = _rtYawNode->getJointValue();
        const float currentPitchAngle = _rtPitchNode->getJointValue();

        const Eigen::Vector3f targetPoint = target.toRootEigen(_rtRobot);

        const float h = _rtCameraNode->getPositionInRootFrame().z();
        float targetYawAngle = -std::atan2(targetPoint.x(), targetPoint.y());
        float targetPitchAngle = std::atan2(h - targetPoint.z(), targetPoint.y());

        // check reachability
        const bool targetReachable = _rtYawNode->checkJointLimits(targetYawAngle) &&
                                     _rtPitchNode->checkJointLimits(targetPitchAngle);
        if (not targetReachable)
        {
            // limit joint ranges to avoid damage
            targetYawAngle = std::clamp(
                targetYawAngle, _rtYawNode->getJointLimitLow(), _rtYawNode->getJointLimitHigh());
            targetPitchAngle = std::clamp(targetPitchAngle,
                                          _rtPitchNode->getJointLimitLow(),
                                          _rtPitchNode->getJointLimitHigh());
        }

        // check reached status

        // TODO implement this in scheduler:

        // const float yawDiff = targetYawAngle - currentYawAngle;
        // const float pitchDiff = targetPitchAngle - currentPitchAngle;
        // const bool targetReached = std::abs(yawDiff) < configBuffer.getUpToDateReadBuffer().params.yawAngleTolerance &&
        //                             std::abs(pitchDiff) < configBuffer.getUpToDateReadBuffer().params.pitchAngleTolerance;
        // if (targetReached)
        // {
        //     // update timestamp on first time reached
        //     if (targetReachable && !_currentTarget.isReached())
        //     {
        //         _currentTarget.reachedTimestamp = now;
        //         _currentTarget.status = gaze_targets::TargetStatus::Fixating;
        //         _tripRt2NonRt.getWriteBuffer() = _currentTarget;
        //         _tripRt2NonRt.commitWrite();
        //     }
        //     // keep tracking until target is stopped
        // }

        // report debugging variables
        _publishCurrentYawAngle = currentYawAngle;
        _publishCurrentPitchAngle = currentPitchAngle;
        _publishTargetYawAngle = targetYawAngle;
        _publishTargetPitchAngle = targetPitchAngle;

        // update control positions
        _rtYawCtrlTarget->position = targetYawAngle;
        _rtPitchCtrlTarget->position = targetPitchAngle;
    }

    void
    GazeController::rtPreActivateController()
    {
    }

    void
    GazeController::rtPostDeactivateController()
    {
    }

    void
    GazeController::onPublish(const SensorAndControl&,
                              const DebugDrawerInterfacePrx&,
                              const DebugObserverInterfacePrx& debugObserver)
    {

        const float currentPitchAngle = _publishCurrentPitchAngle;
        const float currentYawAngle = _publishCurrentYawAngle;
        const float targetPitchAngle = _publishTargetPitchAngle;
        const float targetYawAngle = _publishTargetYawAngle;

        StringVariantBaseMap datafields;
        datafields["currentPitchAngle"] = new Variant(currentPitchAngle);
        datafields["currentYawAngle"] = new Variant(currentYawAngle);
        datafields["targetPitchAngle"] = new Variant(targetPitchAngle);
        datafields["targetYawAngle"] = new Variant(targetYawAngle);
        if (_tripRt2NonRt.updateReadBuffer())
        {
            gaze_targets::GazeTarget target = _tripRt2NonRt.getReadBuffer();
            // this->publishTarget(target);
        }
        debugObserver->setDebugChannel(getInstanceName(), datafields);
    }

} // namespace armarx::view_selection::gaze_controller
