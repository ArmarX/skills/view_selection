/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

namespace armarx::view_selection::gaze_controller
{

    struct Config;
    struct Params;
    struct Targets;

    namespace arondto
    {
        class Config;
        class Params;
        class Targets;
    } // namespace arondto

    void fromAron(const arondto::Config& dto, Config& bo);
    void toAron(arondto::Config& dto, const Config& bo);

    void fromAron(const arondto::Params& dto, Params& bo);
    void toAron(arondto::Params& dto, const Params& bo);

    void fromAron(const arondto::Targets& dto, Targets& bo);
    void toAron(arondto::Targets& dto, const Targets& bo);

} // namespace armarx::view_selection::gaze_controller
