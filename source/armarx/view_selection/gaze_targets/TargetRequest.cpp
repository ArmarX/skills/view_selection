/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_view_selection::ArmarXObjects::object
 * @author     Johann Mantel
 * @date       2nullptr22
 * @copyright  http://www.gnu.org/licenses/gpl-2.nullptr.txt
 *             GNU General Public License
 */

#include "TargetRequest.h"

namespace armarx::view_selection::gaze_targets
{

    TargetRequest::TargetRequest(std::string targetName,
                                 const FramedPosition& targetPosition,
                                 const TargetPriority& priority,
                                 armarx::core::time::Duration duration,
                                 const bool keepInQueue) :
        targetName(targetName),
        targetPosition(targetPosition),
        priority(priority),
        duration(duration),
        keepInQueue(keepInQueue)
    {
        submissionTimestamp = core::time::DateTime::Now();
        status = TargetStatus::Requested;
    }

    // const std::string TargetRequest::getName() { return this->target.targetName;
    // };
} // namespace armarx::view_selection::gaze_targets
