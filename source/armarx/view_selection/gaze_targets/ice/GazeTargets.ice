#pragma once

#include <ArmarXCore/interface/core/time.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>

module armarx {
  module view_selection {
    module gaze_targets {
        module dto {
          module AttentionType {
            enum AttentionTypeEnum {
              TaskDriven = 2,
              StimulusDriven = 1,
              RandomEvent = 0
            };
          };

          struct TargetPriority {
            AttentionType::AttentionTypeEnum attentionType;
            double priority;
          };

          class GazeTarget {
            string targetName;
            armarx::core::time::dto::DateTime requestTimestamp;
            armarx::core::time::dto::DateTime reachedTimestamp;
            armarx::core::time::dto::DateTime releasedTimestamp;
            armarx::core::time::dto::DateTime abortedTimestamp;
            armarx::FramedPositionBase targetPosition;
          };
        };
    };
  };
};
