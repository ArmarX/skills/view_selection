#pragma once
#include "AttentionType.h"
#include "GazeTarget.h"
#include "TargetPriority.h"
#include <armarx/view_selection/gaze_targets/ice/GazeTargets.h>

namespace armarx::view_selection::gaze_targets
{

    void fromIce(const dto::GazeTargetPtr& dto, GazeTarget& bo);
    void toIce(dto::GazeTargetPtr& dto, const GazeTarget& bo);

    void fromIce(const dto::AttentionType::AttentionTypeEnum& dto, AttentionType& bo);
    void toIce(dto::AttentionType::AttentionTypeEnum& dto, const AttentionType& bo);

    void fromIce(const dto::TargetPriority& dto, TargetPriority& bo);
    void toIce(dto::TargetPriority& dto, const TargetPriority& bo);

} // namespace armarx::view_selection::gaze_targets
