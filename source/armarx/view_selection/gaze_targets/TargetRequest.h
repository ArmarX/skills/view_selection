/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::client
 * @author     Johann Mantel
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/time.h>

#include <RobotAPI/libraries/core/FramedPose.h>

#include <armarx/view_selection/gaze_targets/GazeTarget.h>
#include <armarx/view_selection/gaze_targets/TargetPriority.h>
#include <armarx/view_selection/gaze_targets/TargetStatus.h>

namespace armarx::view_selection::gaze_targets
{
    /**
 * @brief Business Object (BO) class of TargetRequest.
 */
    class TargetRequest
    {
    public:
        std::string targetName;
        FramedPosition targetPosition;
        TargetPriority priority;
        armarx::core::time::Duration duration;
        bool keepInQueue;
        core::time::DateTime submissionTimestamp;
        TargetStatus status;

    public:
        TargetRequest(std::string targetName,
                      const FramedPosition& targetPosition,
                      const TargetPriority& priority,
                      armarx::core::time::Duration duration,
                      const bool keepInQueue);

        // const std::string getName();

        inline bool
        operator<(const TargetRequest& rhs) const
        {
            return priority < rhs.priority;
        };

        inline bool
        operator<=(const TargetRequest& rhs) const
        {
            return priority <= rhs.priority;
        };

        inline bool
        operator==(const TargetRequest& rhs) const
        {
            return priority == rhs.priority;
        };

        inline bool
        operator!=(const TargetRequest& rhs) const
        {
            return priority != rhs.priority;
        };

        inline bool
        operator>=(const TargetRequest& rhs) const
        {
            return priority >= rhs.priority;
        };

        inline bool
        operator>(const TargetRequest& rhs) const
        {
            return priority > rhs.priority;
        };

    } // namespace armarx::view_selection::gaze_targets
