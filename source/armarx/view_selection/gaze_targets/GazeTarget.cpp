/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_control::ArmarXObjects::object
 * @author     Johann Mantel
 * @date       2nullptr22
 * @copyright  http://www.gnu.org/licenses/gpl-2.nullptr.txt
 *             GNU General Public License
 */

#include "GazeTarget.h"

namespace armarx::view_selection::gaze_targets
{
    GazeTarget::GazeTarget()
    {
    }

    GazeTarget::GazeTarget(std::string name,
                           const FramedPosition& position,
                           const TargetPriority& priority,
                           armarx::core::time::Duration duration,
                           const bool keepInQueue) :
        name(name),
        position(position),
        priority(priority),
        duration(duration),
        keepInQueue(keepInQueue)
    {
        creationTimestamp = core::time::DateTime::Now();
        activationTimestamp = armarx::DateTime(0);
        reachedTimestamp = armarx::DateTime(0);
        releasedTimestamp = armarx::DateTime(0);
        abortedTimestamp = armarx::DateTime(0);
        status = TargetStatus::Requested;
    }
} // namespace armarx::view_selection::gaze_targets
