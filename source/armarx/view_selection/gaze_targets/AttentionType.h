/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::objects
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once
#include <SimoxUtility/meta/enum/EnumNames.hpp>

namespace armarx::view_selection::gaze_targets
{
    /**
 * @brief Describes the type of attention.
 */
    enum class AttentionType
    {
        /// Task-Driven attention has highest priority
        TaskDriven = 2,
        /// Stimulus-Driven attention is executed when there is no Task-Driven
        /// GazeTarget
        StimulusDriven = 1,
        /// Random Targets with lowest priority
        RandomEvent = 0
    };

    inline const simox::meta::EnumNames<AttentionType> AttentionTypeNames{
        {AttentionType::TaskDriven, "TaskDriven"},
        {AttentionType::StimulusDriven, "StimulusDriven"},
        {AttentionType::RandomEvent, "RandomEvent"}};

} // namespace armarx::view_selection::gaze_targets
