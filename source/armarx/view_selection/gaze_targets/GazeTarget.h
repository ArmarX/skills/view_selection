/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_control::ArmarXObjects::object
 * @author     Johann Mantel
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/time.h>

#include <RobotAPI/libraries/core/FramedPose.h>

#include <armarx/view_selection/gaze_targets/TargetPriority.h>
#include <armarx/view_selection/gaze_targets/TargetStatus.h>

namespace armarx::view_selection::gaze_targets
{
    /**
    * @brief Business Object (BO) class of GazeTarget.
    */
    class GazeTarget
    {
    public:
        // General properties
        std::string name = "";
        FramedPosition position;
        // Scheduler values
        TargetPriority priority = TargetPriority(AttentionType::RandomEvent, 0.0);
        armarx::core::time::Duration duration = armarx::Duration::MicroSeconds(0);
        bool keepInQueue = false;
        // Event logging
        TargetStatus status = TargetStatus::Requested;
        armarx::DateTime creationTimestamp = armarx::DateTime(0);
        armarx::DateTime activationTimestamp = armarx::DateTime(0);
        armarx::DateTime reachedTimestamp = armarx::DateTime(0);
        armarx::DateTime releasedTimestamp = armarx::DateTime(0);
        armarx::DateTime abortedTimestamp = armarx::DateTime(0);

        GazeTarget();
        explicit GazeTarget(std::string targetName,
                            const FramedPosition& targetPosition,
                            const TargetPriority& priority,
                            armarx::core::time::Duration duration,
                            const bool keepInQueue);

        bool
        isActive() const
        {
            return activationTimestamp.toMicroSecondsSinceEpoch() > 0;
        }
        bool
        isReached() const
        {
            return reachedTimestamp.toMicroSecondsSinceEpoch() > 0;
        }
        bool
        isReleased() const
        {
            return releasedTimestamp.toMicroSecondsSinceEpoch() > 0;
        }
        bool
        isAborted() const
        {
            return abortedTimestamp.toMicroSecondsSinceEpoch() > 0;
        }

        inline bool
        operator<(const GazeTarget& rhs) const
        {
            return priority < rhs.priority;
        };

        inline bool
        operator<=(const GazeTarget& rhs) const
        {
            return priority <= rhs.priority;
        };

        inline bool
        operator==(const GazeTarget& rhs) const
        {
            return priority == rhs.priority;
        };

        inline bool
        operator!=(const GazeTarget& rhs) const
        {
            return priority != rhs.priority;
        };

        inline bool
        operator>=(const GazeTarget& rhs) const
        {
            return priority >= rhs.priority;
        };

        inline bool
        operator>(const GazeTarget& rhs) const
        {
            return priority > rhs.priority;
        };

        friend std::ostream&
        operator<<(std::ostream& os, const GazeTarget& target)
        {
            return os << "{ " << target.name << ", " << target.creationTimestamp << ", "
                      << target.priority << ", " << target.duration << ", "
                      << TargetStatusNames.to_name(target.status) << " }";
        };
    };

} // namespace armarx::view_selection::gaze_targets
