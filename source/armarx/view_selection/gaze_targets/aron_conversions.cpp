/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    gaze_controller::ArmarXObjects::object
 * @author     Johann Mantel
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "aron_conversions.h"

namespace armarx::view_selection::gaze_targets
{

    void
    toAron(arondto::AttentionType& dto, const AttentionType& bo)
    {
        switch (bo)
        {
            case AttentionType::TaskDriven:
                dto.value = arondto::AttentionType::TaskDriven;
                break;
            case AttentionType::StimulusDriven:
                dto.value = arondto::AttentionType::StimulusDriven;
                break;
            case AttentionType::RandomEvent:
                dto.value = arondto::AttentionType::RandomEvent;
                break;
        }
    }

    void
    fromAron(const arondto::AttentionType& dto, AttentionType& bo)
    {
        switch (dto.value)
        {
            case arondto::AttentionType::TaskDriven:
                bo = AttentionType::TaskDriven;
                break;
            case arondto::AttentionType::StimulusDriven:
                bo = AttentionType::StimulusDriven;
                break;
            case arondto::AttentionType::RandomEvent:
                bo = AttentionType::RandomEvent;
                break;
        }
    }

    void
    toAron(arondto::TargetPriority& dto, const TargetPriority& bo)
    {
        toAron(dto.attentionType, bo.attentionType);
        dto.priority = bo.priority;
    }
    void
    fromAron(const arondto::TargetPriority& dto, TargetPriority& bo)
    {
        fromAron(dto.attentionType, bo.attentionType);
        bo.priority = dto.priority;
    }

    void
    toAron(arondto::TargetStatus& dto, const TargetStatus& bo)
    {
        switch (bo)
        {
            case TargetStatus::Requested:
                dto.value = arondto::TargetStatus::Requested;
                break;
            case TargetStatus::Active:
                dto.value = arondto::TargetStatus::Active;
                break;
            case TargetStatus::Fixating:
                dto.value = arondto::TargetStatus::Fixating;
                break;
            case TargetStatus::Done:
                dto.value = arondto::TargetStatus::Done;
                break;
            case TargetStatus::Aborted:
                dto.value = arondto::TargetStatus::Aborted;
                break;
        }
    }

    void
    fromAron(const arondto::TargetStatus& dto, TargetStatus& bo)
    {
        switch (dto.value)
        {
            case arondto::TargetStatus::Requested:
                bo = TargetStatus::Requested;
                break;
            case arondto::TargetStatus::Active:
                bo = TargetStatus::Active;
                break;
            case arondto::TargetStatus::Fixating:
                bo = TargetStatus::Fixating;
                break;
            case arondto::TargetStatus::Done:
                bo = TargetStatus::Done;
                break;
            case arondto::TargetStatus::Aborted:
                bo = TargetStatus::Aborted;
                break;
        }
    }

    void
    toAron(arondto::GazeTarget& dto, const GazeTarget& bo)
    {
        dto.name = bo.name;
        toAron(dto.position, bo.position);
        toAron(dto.priority, bo.priority);
        armarx::toAron(dto.duration, bo.duration);
        dto.keepInQueue = bo.keepInQueue;
        toAron(dto.status, bo.status);
        armarx::toAron(dto.creationTimestamp, bo.creationTimestamp);
        armarx::toAron(dto.activationTimestamp, bo.activationTimestamp);
        armarx::toAron(dto.reachedTimestamp, bo.reachedTimestamp);
        armarx::toAron(dto.releasedTimestamp, bo.releasedTimestamp);
        armarx::toAron(dto.abortedTimestamp, bo.abortedTimestamp);
    }

    void
    fromAron(const arondto::GazeTarget& dto, GazeTarget& bo)
    {
        bo.name = dto.name;
        fromAron(dto.position, bo.position);
        fromAron(dto.priority, bo.priority);
        armarx::fromAron(dto.duration, bo.duration);
        bo.keepInQueue = dto.keepInQueue;
        fromAron(dto.status, bo.status);
        armarx::fromAron(dto.creationTimestamp, bo.creationTimestamp);
        armarx::fromAron(dto.activationTimestamp, bo.activationTimestamp);
        armarx::fromAron(dto.reachedTimestamp, bo.reachedTimestamp);
        armarx::fromAron(dto.releasedTimestamp, bo.releasedTimestamp);
        armarx::fromAron(dto.abortedTimestamp, bo.abortedTimestamp);
    }

} // namespace armarx::view_selection::gaze_targets
