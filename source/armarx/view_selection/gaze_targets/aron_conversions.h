/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    memory_tutorial::ArmarXObjects::object
 * @author     Johann Mantel
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/time.h>

#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/armarx.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/framed.h>
#include <RobotAPI/libraries/aron/common/aron_conversions/simox.h>

#include <armarx/view_selection/gaze_targets/AttentionType.h>
#include <armarx/view_selection/gaze_targets/GazeTarget.h>
#include <armarx/view_selection/gaze_targets/TargetPriority.h>
#include <armarx/view_selection/gaze_targets/TargetStatus.h>
#include <armarx/view_selection/gaze_targets/aron/AttentionType.aron.generated.h>
#include <armarx/view_selection/gaze_targets/aron/GazeTarget.aron.generated.h>
#include <armarx/view_selection/gaze_targets/aron/TargetPriority.aron.generated.h>
#include <armarx/view_selection/gaze_targets/aron/TargetStatus.aron.generated.h>

namespace armarx
{

    template <typename AronType, typename T>
    T
    fromAron(const ::armarx::aron::data::dto::DictPtr& dto)
    {
        static_assert(
            std::is_base_of<armarx::aron::codegenerator::cpp::AronGeneratedClass, AronType>::value,
            "AronType must be an ARON generated type");

        const auto dtoConfig = AronType::FromAron(dto);

        T config;
        fromAron(dtoConfig, config);

        return config;
    }
} // namespace armarx

namespace armarx::view_selection::gaze_targets
{

    // void toAron(arondto::FramedPosition &dto, const armarx::FramedPositionPtr
    // &bo); void fromAron(const arondto::FramedPosition &dto,
    //              armarx::FramedPositionPtr &bo);

    void toAron(arondto::AttentionType& dto, const AttentionType& bo);
    void fromAron(const arondto::AttentionType& dto, AttentionType& bo);

    void toAron(arondto::TargetPriority& dto, const TargetPriority& bo);
    void fromAron(const arondto::TargetPriority& dto, TargetPriority& bo);

    void toAron(arondto::TargetStatus& dto, const TargetStatus& bo);
    void fromAron(const arondto::TargetStatus& dto, TargetStatus& bo);

    void toAron(arondto::GazeTarget& dto, const GazeTarget& bo);
    void fromAron(const arondto::GazeTarget& dto, GazeTarget& bo);

} // namespace armarx::view_selection::gaze_targets
