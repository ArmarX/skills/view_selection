/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::client
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TargetPriority.h"

#include <iostream>

namespace armarx::view_selection::gaze_targets
{
    TargetPriority::TargetPriority(const AttentionType& attentionType, double priority) :
        attentionType(attentionType), priority(priority)
    {
    }

    bool
    TargetPriority::operator<(const TargetPriority& rhs) const
    {
        return attentionType < rhs.attentionType ||
               (attentionType == rhs.attentionType && priority < rhs.priority);
    }

    bool
    TargetPriority::operator<=(const TargetPriority& rhs) const
    {
        return attentionType < rhs.attentionType ||
               (attentionType == rhs.attentionType && priority <= rhs.priority);
    }

    bool
    TargetPriority::operator==(const TargetPriority& rhs) const
    {
        return attentionType == rhs.attentionType && priority == rhs.priority;
    }

    bool
    TargetPriority::operator!=(const TargetPriority& rhs) const
    {
        return attentionType != rhs.attentionType || priority != rhs.priority;
    }

    bool
    TargetPriority::operator>=(const TargetPriority& rhs) const
    {
        return attentionType > rhs.attentionType ||
               (attentionType == rhs.attentionType && priority >= rhs.priority);
    }

    bool
    TargetPriority::operator>(const TargetPriority& rhs) const
    {
        return attentionType > rhs.attentionType ||
               (attentionType == rhs.attentionType && priority > rhs.priority);
    }

    std::ostream&
    operator<<(std::ostream& os, const TargetPriority& prio)
    {
        return os << "{ " << AttentionTypeNames.to_name(prio.attentionType) << ", " << prio.priority
                  << " }";
    }

} // namespace armarx::view_selection::gaze_targets
