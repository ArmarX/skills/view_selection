#include "ice_conversions.h"

#include <ArmarXCore/core/ice_conversions.h>
#include <ArmarXCore/core/time.h>

namespace armarx::view_selection::gaze_targets
{

    void
    fromIce(const dto::GazeTargetPtr& dto, GazeTarget& bo)
    {
        bo.targetName = dto->targetName;
        fromIce(dto->requestTimestamp, bo.requestTimestamp);
        fromIce(dto->reachedTimestamp, bo.reachedTimestamp);
        fromIce(dto->releasedTimestamp, bo.releasedTimestamp);
        fromIce(dto->abortedTimestamp, bo.abortedTimestamp);
        // fromIce(dto->targetPosition, bo.targetPosition);
    }

    void
    toIce(dto::GazeTargetPtr& dto, const GazeTarget& bo)
    {
        dto->targetName = bo.targetName;
        toIce(dto->requestTimestamp, bo.requestTimestamp);
        toIce(dto->reachedTimestamp, bo.reachedTimestamp);
        toIce(dto->releasedTimestamp, bo.releasedTimestamp);
        toIce(dto->abortedTimestamp, bo.abortedTimestamp);
        // toIce(dto->targetPosition, bo.targetPosition);
    }

    void
    fromIce(const dto::AttentionType::AttentionTypeEnum& dto, AttentionType& bo)
    {
        switch (dto)
        {
            case dto::AttentionType::TaskDriven:
                bo = AttentionType::TaskDriven;
                break;
            case dto::AttentionType::StimulusDriven:
                bo = AttentionType::StimulusDriven;
                break;
            case dto::AttentionType::RandomEvent:
                bo = AttentionType::RandomEvent;
                break;
        }
    }

    void
    toIce(dto::AttentionType::AttentionTypeEnum& dto, const AttentionType& bo)
    {
        switch (bo)
        {
            case AttentionType::TaskDriven:
                dto = dto::AttentionType::TaskDriven;
                break;
            case AttentionType::StimulusDriven:
                dto = dto::AttentionType::StimulusDriven;
                break;
            case AttentionType::RandomEvent:
                dto = dto::AttentionType::RandomEvent;
                break;
        }
    }

    void
    fromIce(const dto::TargetPriority& dto, TargetPriority& bo)
    {
        AttentionType attentionType;
        fromIce(dto.attentionType, attentionType);
        bo = TargetPriority(attentionType, dto.priority);
    }

    void
    toIce(dto::TargetPriority& dto, const TargetPriority& bo)
    {
        toIce(dto.attentionType, bo.attentionType);
        dto.priority = bo.priority;
    }

} // namespace armarx::view_selection::gaze_targets
