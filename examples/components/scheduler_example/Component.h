/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::scheduler_example
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/plugins.h>
// for writing
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>

#include <armarx/view_selection/client/ViewSelection.h>
#include <armarx/view_selection/gaze_targets.h>

namespace armarx::view_selection::components::scheduler_example
{
    //
    class Component :
        virtual public armarx::Component,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armem::ListeningClientPluginUser
    {
    public:
        Component();
        ~Component();
        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
        /// Get the component's default name.
        static std::string GetDefaultName();

    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;
        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;
        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;
        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

    private:
        // Private methods go here.
        void testPriorityScheduling();
        void testTargetReplacement();

    private:
        static const std::string defaultName;
        // Private member variables go here.
        // gaze_targets::GazeSchedulerInterfacePrx gazeScheduler;
        armem::robot_state::VirtualRobotReader virtualRobotReader;
        client::ViewSelection viewSelection;
        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            std::string RobotUnitName = "Armar6Unit";
            std::string robotName = "Armar6";
            std::string robotFrame = "root";
            std::string positionLayerName = "TargetPositions";
        };
        Properties properties;
    };

} // namespace armarx::view_selection::components::scheduler_example
