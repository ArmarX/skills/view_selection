/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::scheduler_example
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Component.h"

#include <ArmarXCore/core/time.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include "RobotAPI/libraries/core/FramedPose.h"

namespace armarx::view_selection::components::scheduler_example
{

    const std::string Component::defaultName = "scheduler_example";

    Component::Component() :
        virtualRobotReader(memoryNameSystem()), viewSelection(memoryNameSystem(), getName())
    {
    }

    Component::~Component() = default;

    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(gazeScheduler);

        // Add a required property. (The component won't start without a value being
        // set.) def->required(properties.boxLayerName, "p.box.LayerName", "Name of
        // the box layer in ArViz.");

        def->optional(
            properties.RobotUnitName, "properties.RobotUnitName", "The name of the RobotUnit");
        def->optional(
            properties.robotName, "properties.RobotName", "Name of the robot for framedPosition.");
        def->optional(properties.robotFrame,
                      "properties.robotFrameName",
                      "Name of the local robotFrame for framedPosition.");
        def->optional(properties.positionLayerName,
                      "properties.position.LayerName",
                      "Name of the layer in ArViz for targetPositions.");

        virtualRobotReader.registerPropertyDefinitions(def);
        return def;
    }

    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.
    }

    void
    Component::onConnectComponent()
    {
        virtualRobotReader.connect();
        viewSelection.connect();

        testPriorityScheduling();
        //testTargetReplacement();
    }

    void
    Component::onDisconnectComponent()
    {
    }

    void
    Component::onExitComponent()
    {
    }

    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }

    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }

    void
    Component::testPriorityScheduling()
    {
        ARMARX_INFO << "Testing Scheduler with targets of different Priorities, in "
                    << "both global (blue bullets) and root (orange bullets) frame.";

        namespace gaze_targets = armarx::view_selection::gaze_targets;
        // create positions to target
        VirtualRobot::RobotPtr robotPtr =
            virtualRobotReader.getSynchronizedRobot(properties.robotName, core::time::Clock::Now());
        ARMARX_CHECK_NOT_NULL(robotPtr);


        ARMARX_INFO << "The first target (TaskDriven, 0.4) will be replaced by the "
                    << "second one (TaskDriven, 1.5)";

        gaze_targets::GazeTarget target1;
        target1.name = "target1";
        target1.position = FramedPosition(
            Eigen::Vector3f(0, 4000, 2000), properties.robotFrame, properties.robotName);
        target1.priority =
            gaze_targets::TargetPriority(gaze_targets::AttentionType::TaskDriven, 0.4);
        target1.duration = armarx::Duration::MilliSeconds(1000);
        target1.creationTimestamp = armarx::Clock::Now();
        viewSelection.commitGazeTarget(target1);

        gaze_targets::GazeTarget target2;
        target2.name = "target2global";
        target2.position = FramedPosition(
            Eigen::Vector3f(500, 2000, 1000), properties.robotFrame, properties.robotName);
        target2.position.changeToGlobal(robotPtr);
        target2.priority =
            gaze_targets::TargetPriority(gaze_targets::AttentionType::TaskDriven, 1.5);
        target2.duration = armarx::Duration::MilliSeconds(1000);
        target2.creationTimestamp = armarx::Clock::Now();
        viewSelection.commitGazeTarget(target2);

        ARMARX_INFO << "The following tasks will be in order of Priority (no matter "
                    << "the subission order) after the second one is finished";
        ARMARX_INFO << "Each goal is fixated for one second after it is reached";

        gaze_targets::GazeTarget target3;
        target3.name = "target3global";
        target3.position = FramedPosition(
            Eigen::Vector3f(500, 1000, 1000), properties.robotFrame, properties.robotName);
        target3.position.changeToGlobal(robotPtr);
        target3.priority =
            gaze_targets::TargetPriority(gaze_targets::AttentionType::TaskDriven, 0.5);
        target3.duration = armarx::Duration::MilliSeconds(1000);
        target3.creationTimestamp = armarx::Clock::Now();
        viewSelection.commitGazeTarget(target3);

        gaze_targets::GazeTarget target4;
        target4.name = "target4";
        target4.position = FramedPosition(
            Eigen::Vector3f(-500, 2000, 1000), properties.robotFrame, properties.robotName);
        target4.priority =
            gaze_targets::TargetPriority(gaze_targets::AttentionType::TaskDriven, 0.6);
        target4.duration = armarx::Duration::MilliSeconds(1000);
        target4.creationTimestamp = armarx::Clock::Now();
        viewSelection.commitGazeTarget(target4);

        gaze_targets::GazeTarget target5;
        target5.name = "target5";
        target5.position = FramedPosition(
            Eigen::Vector3f(1500, 2000, 1000), properties.robotFrame, properties.robotName);
        target5.priority =
            gaze_targets::TargetPriority(gaze_targets::AttentionType::TaskDriven, 0.7);
        target5.duration = armarx::Duration::MilliSeconds(1000);
        target5.creationTimestamp = armarx::Clock::Now();
        viewSelection.commitGazeTarget(target5);

        // test infinite duration and global target
        armarx::TimeUtil::Sleep(0.5);
        ARMARX_INFO << "The last goal (StimulusDriven, 10) will be executed last and "
                    << "fixate its targetPoint infinitely until a goal with higher "
                       "priority "
                    << "is submitted, i.e. by restarting this test";
        ARMARX_INFO << "You can test the fixation by moving the platform via the "
                       ">>PlatformUnitGui<<";

        gaze_targets::GazeTarget targetInfinite;
        targetInfinite.name = "targetInfinite";
        targetInfinite.position = FramedPosition(
            Eigen::Vector3f(1500, 2000, 1000), properties.robotFrame, properties.robotName);
        targetInfinite.position.changeToGlobal(robotPtr);
        targetInfinite.priority =
            gaze_targets::TargetPriority(gaze_targets::AttentionType::StimulusDriven, 10);
        //a negative Duration will fixate until target with higher priority is submitted
        targetInfinite.duration = armarx::Duration::MilliSeconds(-1000);
        targetInfinite.creationTimestamp = armarx::Clock::Now();
        viewSelection.commitGazeTarget(targetInfinite);

        // visualization
        {
            viz::Layer layer = arviz.layer(properties.positionLayerName);

            layer.add(viz::Sphere(target1.name)
                          .position(target1.position.toGlobalEigen(robotPtr))
                          .radius(50)
                          .color(simox::Color(255, 224, 80)));
            layer.add(viz::Sphere(target2.name)
                          .position(target2.position.toGlobalEigen(robotPtr))
                          .radius(50)
                          .color(simox::Color::blue()));
            layer.add(viz::Sphere(target3.name)
                          .position(target3.position.toGlobalEigen(robotPtr))
                          .radius(50)
                          .color(simox::Color::blue()));
            layer.add(viz::Sphere(target4.name)
                          .position(target4.position.toGlobalEigen(robotPtr))
                          .radius(50)
                          .color(simox::Color(255, 128, 80)));
            layer.add(viz::Sphere(target5.name)
                          .position(target5.position.toGlobalEigen(robotPtr))
                          .radius(50)
                          .color(simox::Color(255, 96, 80)));
            layer.add(viz::Sphere(targetInfinite.name)
                          .position(targetInfinite.position.toGlobalEigen(robotPtr))
                          .radius(50)
                          .color(simox::Color::blue()));
            arviz.commit(layer);
        }
    }

    void
    Component::testTargetReplacement()
    {
        // test target updating
        ARMARX_INFO << "Submitting several targets with the same name will replace the "
                    << "waiting requests or even active targets.";

        gaze_targets::GazeTarget target1;
        target1.name = "target1";
        target1.position = FramedPosition(
            Eigen::Vector3f(-500, 1000, 1000), properties.robotFrame, properties.robotName);
        target1.priority =
            gaze_targets::TargetPriority(gaze_targets::AttentionType::TaskDriven, 10);
        target1.duration = armarx::Duration::MilliSeconds(-1000);
        target1.creationTimestamp = armarx::Clock::Now();
        viewSelection.commitGazeTarget(target1);

        gaze_targets::GazeTarget target2;
        target2.name = "target2";
        target2.priority =
            gaze_targets::TargetPriority(gaze_targets::AttentionType::StimulusDriven, 1);
        target2.duration = armarx::Duration::MilliSeconds(1000);
        target2.creationTimestamp = armarx::Clock::Now();

        for (int x = -1000; x < 1000; x += 500)
        {
            //change the position
            target2.position = FramedPosition(
                Eigen::Vector3f(x, 2000, 1000), properties.robotFrame, properties.robotName);
            viewSelection.commitGazeTarget(target2);
            armarx::TimeUtil::Sleep(0.5);
        }

        ARMARX_INFO << "The target1 still blocks the execution of target2, "
                    << "let us stop it by changing its duration";

        target1.duration = armarx::Duration::MilliSeconds(0);
        viewSelection.commitGazeTarget(target1);
    }

    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::view_selection::components::scheduler_example
