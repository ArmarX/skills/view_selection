/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::object_tracking_example
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ArmarXCore/core/services/tasks/TaskUtil.h"
#include <ArmarXCore/core/Component.h>

#include "RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h"
// #include
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/client/plugins.h>
// for writing
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>

#include <armarx/view_selection/client/ViewSelection.h>
#include <armarx/view_selection/gaze_targets.h>

namespace armarx::view_selection::components::object_tracking_example
{
    class Component :
        virtual public armarx::Component,
        virtual public ObjectPoseClientPluginUser,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armem::ListeningClientPluginUser
    {
    public:
        Component();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        /// Get the component's default name.
        static std::string GetDefaultName();

    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

    private:
        void testObjectTrackingPeriodically();

    private:
        static const std::string defaultName;
        // Private member variables go here.

        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            std::string targetLayerName = "targets";
            std::string objectProviderDynamic = "ivt_object_localization";
            std::string robotName = "Armar6";
            std::string robotBaseFrame = "root";

            float objectDetectionTimeout = 3.f;
        };
        Properties properties;

        armarx::SimplePeriodicTask<>::pointer_type task;

        armem::robot_state::VirtualRobotReader virtualRobotReader;
        client::ViewSelection viewSelection;
    };

} // namespace armarx::view_selection::components::object_tracking_example
