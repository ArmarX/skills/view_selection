/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    view_selection::ArmarXObjects::scheduler_example
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Component.h"

#include <thread>

#include "ArmarXCore/core/logging/Logging.h"
#include "ArmarXCore/core/services/tasks/TaskUtil.h"
#include <ArmarXCore/core/time.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

#include "RobotAPI/components/ArViz/Client/Elements.h"
#include "RobotAPI/components/ArViz/Client/elements/Color.h"
#include "RobotAPI/libraries/core/FramedPose.h"

// Include headers you only need in function definitions in the .cpp.

// #include <Eigen/Core>

// #include <SimoxUtility/color/Color.h>

namespace armarx::view_selection::components::object_tracking_example
{

    const std::string Component::defaultName = "object_tracking_example";

    Component::Component() :
        virtualRobotReader(memoryNameSystem()), viewSelection(memoryNameSystem(), getName())
    {
    }

    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Add an optionalproperty.
        def->optional(
            properties.targetLayerName, "p.target.LayerName", "Name of the target layer in ArViz.");
        def->optional(properties.objectProviderDynamic, "p.objectProviderDynamic", "");
        def->optional(properties.robotName, "p.robotName", "");

        virtualRobotReader.registerPropertyDefinitions(def);

        return def;
    }

    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.
    }

    void
    Component::onConnectComponent()
    {
        virtualRobotReader.connect();
        viewSelection.connect();

        // initialize. low prio: look straight

        ARMARX_INFO << "Periodically refreshing the pose of nesquik box and updating "
                    << "the respective target.\nThe robot is expected to keep it in sight.";
        task = new armarx::SimplePeriodicTask<>([&]() { testObjectTrackingPeriodically(); }, 100);
        task->start();
    }

    void
    Component::onDisconnectComponent()
    {
        task->stop();
    }

    void
    Component::onExitComponent()
    {
    }

    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }

    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }

    std::optional<objpose::ObjectPose>
    findObject(const objpose::ObjectPoseSeq& objectPoses, const armarx::ObjectID& objectID)
    {
        const auto matchesId = [&objectID](const objpose::ObjectPose& objectPose) -> bool
        { return objectPose.objectID == objectID; };

        const auto it = std::find_if(objectPoses.begin(), objectPoses.end(), matchesId);
        if (it != objectPoses.end())
        {
            return *it;
        }

        return std::nullopt;
    }

    void
    Component::testObjectTrackingPeriodically()
    {

        const armarx::objpose::ObjectPoseSeq objectPosesDynamic =
            armarx::ObjectPoseClientPluginUser::getObjectPosesByProvider(
                properties.objectProviderDynamic);
        for (const auto& objPose : objectPosesDynamic)
        {
            ARMARX_INFO << "Object: " << objPose.objectID;
        }

        const armarx::ObjectID nesquikId0("Kitchen", "nesquik", "0");
        const armarx::ObjectID vanillaCookiesId0("Kitchen", "vanilla-cookies", "0");
        const armarx::ObjectID crackerId0("YCB", "003_cracker_box", "0");
        namespace armem = armarx::armem;
        const armem::MemoryID coreSegmentID("View", "GazeTarget");

        // FramedPositionPtr nesquikPosition, cookiePosition;

        //const auto nesquikPose = findObject(objectPosesDynamic, nesquikId0);
        const auto crackerPose = findObject(objectPosesDynamic, crackerId0);
        if (crackerPose.has_value())
        {
            const auto dt = armarx::Clock::Now() - crackerPose->timestamp;

            if (dt.toSecondsDouble() < properties.objectDetectionTimeout)
            {

                FramedPosition crackerPosition(
                    Eigen::Vector3f(Eigen::Isometry3f(crackerPose->objectPoseGlobal).translation()),
                    GlobalFrame,
                    properties.robotName);

                // visualize the target position
                auto robot = virtualRobotReader.getSynchronizedRobot(properties.robotName,
                                                                     armarx::Clock::Now());
                auto l = arviz.layer("objects");
                l.add(armarx::viz::Sphere("nesquik")
                          .position(Eigen::Vector3f{
                              crackerPosition.x, crackerPosition.y, crackerPosition.z})
                          .radius(100)
                          .color(armarx::viz::Color::red()));
                arviz.commit(l);

                gaze_targets::GazeTarget target;
                target.name = "cracker";
                target.position = crackerPosition;
                target.priority =
                    gaze_targets::TargetPriority(gaze_targets::AttentionType::TaskDriven, 10);
                target.duration = armarx::Duration::Seconds(3);
                target.creationTimestamp = armarx::Clock::Now();

                viewSelection.commitGazeTarget(target);
            }
        }
        else
        {
            ARMARX_WARNING << "Object with id `" << crackerId0.str()
                           << "` not found (provider: " << properties.objectProviderDynamic << ")!";
        }

        // const auto cookiePose = findObject(objectPosesDynamic, vanillaCookiesId0);
        // if(cookiePose.has_value()) {

        //     const auto dt = armarx::Clock::Now() - nesquikPose->timestamp;

        //     if(dt.toSecondsDouble() < properties.objectDetectionTimeout)
        //     {
        //         const ::armarx::FramedPositionBasePtr cookiePosition = new
        //         FramedPosition(cookiePose->objectPoseGlobal,
        //                 GlobalFrame, properties.robotName);
        //         gazeScheduler->fixateFor(cookiePosition, {taskAttention, 1.0},
        //         core::time::dto::Duration(0));
        //     }
        // }
        // else
        // {
        //     ARMARX_WARNING << "Object with id `" << vanillaCookiesId0.str()  << "`
        //     not found (provider: " << properties.objectProviderDynamic << ")!";
        // }
    }

    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

} // namespace armarx::view_selection::components::object_tracking_example
