armarx_add_component(object_tracking_example
    SOURCES
        Component.cpp
    HEADERS
        Component.h
    DEPENDENCIES
        ArmarXCore
        RobotAPIComponentPlugins  # For ArViz and other plugins.
        armem_robot
        armem_robot_state
        armarx_view_selection::gaze_targets
        armarx_view_selection::client
    # DEPENDENCIES_LEGACY
        ## Add libraries that do not provide any targets but ${FOO_*} variables.
        # FOO
    # If you need a separate shared component library you can enable it with the following flag.
    # SHARED_COMPONENT_LIBRARY
)
